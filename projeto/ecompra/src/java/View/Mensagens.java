package View;
/**
 * @author Tiago
 */
public class Mensagens {

	public static final String msgErroLogin = "Login ou senha inválidos";
        public static final String msgSucessoIncluirProduto = "Produto incluído com sucesso!";
        public static final String msgSucessoAlterarProduto = "Produto atualizado com sucesso!";
        public static final String msgSucessoExcluirProduto = "Produto excluído com sucesso!";
        public static final String msgErroExcluirProduto = "Produto NÃO foi excluído!";
        public static final String msgErroAlterarProduto = "Produto NÃO foi atualizado!";
        public static final String msgSucessoIncluirCliente = "Cadastro efetuado com sucesso!Faça já seu login!";
        public static final String msgSucessoBloquearCliente = "Cliente bloqueado com sucesso!";
        public static final String msgSucessoAlterarCliente = "Cadastro atualizado com sucesso!";
}
